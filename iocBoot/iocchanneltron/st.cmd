#!../../bin/linux-x86_64/channeltron

< envPaths

cd "${TOP}"

epicsEnvSet("SYS", "$(IOC_SYS)")
epicsEnvSet("DEV", "$(IOC_DEV)")
epicsEnvSet("ENGINEER","Marcel Bajdel")
epicsEnvSet("COM1", "RS232")
epicsEnvSet("STREAM_PROTOCOL_PATH","$(TOP)/protocols")
epicsEnvSet("LOCATION", "$(IOC_SYS)")
epicsEnvSet("ALIVE_SERVER","sissy-serv-04.exp.helmholtz-berlin.de")


## Register all support components
dbLoadDatabase "dbd/channeltron.dbd"
channeltron_registerRecordDeviceDriver pdbbase

epicsEnvSet("MOXAIP", "172.31.182.33")
drvAsynIPPortConfigure("$(COM1)","$(MOXAIP):9004")


set_requestfile_path("$(TOP)/autosave", "")
set_savefile_path("/opt/epics/autosave/")
set_pass1_restoreFile("settings.sav")
set_pass0_restoreFile("settings.sav")
save_restoreSet_DatedBackupFiles(0)
## Load record instances
dbLoadRecords("$(TOP)/db/HighVoltage.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/DeadTime.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/Threshold.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/Interval.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/OVLimit.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/AutoTransmission.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/AnodeCurrent.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/Measurement.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(SYS):,R=$(DEV):asyn,PORT=$(COM1),ADDR=-1,IMAX=0,OMAX=0")
dbLoadRecords("$(ALIVE)/db/alive.db", "P=$(SYS):$(DEV):,RHOST=$(ALIVE_SERVER)" )
#var streamDebug 1

cd "${TOP}/iocBoot/${IOC}"
iocInit

create_monitor_set("settings.req")
create_manual_set("settings.req")

#var streamDebug 1

dbpf $(SYS):$(DEV):Reset-CMD 1
dbpf $(SYS):$(DEV):Auto-SP 0

dbl > /opt/epics/ioc/log/logs.dbl
